# application-modernisation-immersionday

Supporting documentation for Application Modernization Immersion Day.

## Getting started

Download this repository to have access to supplemental files for use with an Application Modernization Immersion Day.

## General Info

You can find additional details of the lab at the following URLs

- [https://ca-immersionday-sso.awsapps.com/start](https://ca-immersionday-sso.awsapps.com/start) - SSO Sign In for Lab Accounts
- [https://app-modernization.workshop.aws/en/](https://app-modernization.workshop.aws/en/) - Lab Instructions for the Workshop


## Lab 2

This lab involves the setup of the of the API Gateway to create a Facade in front of the monolith. In order to stream line this installation you will use the swagger file included to import the APIs. The file is in the Lab 2 folder and is called:

- immersion-day-unicorn-api-swagger.yaml

You can find directions for importing this into API gateway in the PDF in that folder called:

- swagger-api-deployment-instructions.pdf
